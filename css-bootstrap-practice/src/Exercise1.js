import React from "react"
import {Card,Button} from "react-bootstrap"
import styles from './style.css'


class Exercise1 extends React.Component{

    constructor(){
        super()
        this.state={
            cadImgs:[],
            shownImage:"https://d2t1xqejof9utc.cloudfront.net/screenshots/pics/b6dc07999db4d5addea1ccd26f58d483/large.jpg",
            title:"Future CAD Name",
            description:"Future SPECS of Submitted Mechanical piece"
        }

        this.handleClick= this.handleClick.bind(this)
    }

    componentDidMount() {
        fetch("https://api.imgflip.com/get_memes")
            .then(response => response.json())
            .then(response => {
                const {imgs} = response.data
                this.setState({ cadImgs:imgs })
            })
    }

    handleClick(event){
            event.preventDefault()
            const {rand} = this.state.cadImgs[Math.floor(Math.random() * this.state.cadImgs.length)].url
            this.setState({ shownImage: rand })
    }
    
    render(){
        return(
            <div className="cadcontainer">
                    <Card className="cadcontainer" style={{ width: '25rem' }}>
                            <Card.Img variant="top" src={this.state.shownImage} />
                            <Card.Body>
                                <Card.Title>{this.state.title}</Card.Title>
                                <Card.Text>
                                    {this.state.description}
                                </Card.Text>
                            </Card.Body>

                            <div className="cadcontainer">
                                <Button onClick={this.handleClick} variant="primary">Change Image</Button>

                            </div>
                    </Card>
            </div>
        )
    }
}


export default Exercise1