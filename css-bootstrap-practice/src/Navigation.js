import React from "react"
import {Navbar,NavItem,Nav,Button} from "react-bootstrap"
import {BrowserRouter as Router , Route} from "react-router-dom"

import "../node_modules/bootstrap/dist/css/bootstrap.min.css"

import Exercise1 from "./Exercise1"
/*Navigation Bar destined to be composed of practice components 
in order to optimized practice efficiency by reducing new environement setup time*/

class Navigation extends React.Component{
    render(){
        return(
            <Router>
                <Navbar bg="dark" expand="lg">
                   <Navbar.Brand href="/">
                    Navbar
                   </Navbar.Brand>

                   <NavItem className="navbar-nav ml-auto">
                            <Nav className="mr-auto">
                                <Nav.Link href="/Exercise_1">Exercise1</Nav.Link>
                            </Nav>
                    </NavItem>

                </Navbar>

                <Route path="/Exercise_1" exact component ={Exercise1}/>

            </Router>
            
        )
    }
}
export default Navigation