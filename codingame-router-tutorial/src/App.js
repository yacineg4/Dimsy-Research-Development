import React from "react"

import {BrowserRouter as Router , Switch,Route,Link } from "react-router-dom"
import About from "./About"
import Home from "./Home"
import Contact from "./Contact"

class App extends React.Component {
    render(){
        return(
            <Router>
                <div>
                    <h2>React-Router Tutorial</h2>
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <ul className="Navbar-nav mr-auto">
                            <li>
                                <Link to={"/"} className="nav-link">
                                    Home
                                </Link>
                            </li>

                            <li>
                                <Link to={"/contact"} className="nav-link">
                                    Contact
                                </Link>
                            </li>

                            <li>
                                <Link to={"/about"} className="nav-link">
                                    About
                                </Link>
                            </li>
                        </ul>
                    </nav>
                    <hr />
                    <Switch>
                        <Route exact path='/' component={Home}/>
                        <Route exact path='/contact' component={Contact}/>
                        <Route exact path='/about' component={About}/>
                    </Switch>
                </div>
            </Router>
        )
    }
}

export default App