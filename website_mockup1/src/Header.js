import React from "react"
import {Button, Container,Row,Col} from "react-bootstrap"

function Header(){
    return(
        <header>
                <Row>
                    <Col md={6}>
                         {/* Background image not  showing need to Fix */}
                        <div>
                            <h1 style={{textAlign:"left"}}>Get more done with top‑quality freelancers</h1>
                            <br />
                            <p>
                                Upwork is the leading freelancing website where businesses find and work with top
                                freelance marketers, designers, developers, and other professionals.
                            </p>
                            <br />
                            <Button variant="success">  Get Started  </Button>
                            <br />
                        </div>
                    </Col>
                </Row>

        </header>
    )
}

export default Header
