import React from "react"

//Form allowing potential new users to become clients

class GetStartedForm extends React.Component{
    constructor(){
        super()
        //Form asks for users Name , last name and E-address
        this.state = {
            firstName:"",
            lastName:"",
            workEmail:""
        }
    }

    render(){
        return( 
            <div>
                <h2>Sign up for free project quotes</h2>
                <form>
                    <input 
                        name="firstName"
                        value={this.state.firstName}
                        placeholder="First Name ..." 
                    />
                    <br />
                    <input 
                        name="lasttName"
                        value={this.state.lastName}
                        placeholder="Last Name ..." 
                    />
                    <br />
                    <input 
                        name="emailAddress"
                        value={this.state.workEmail}
                        placeholder="Work Email Address ..." 
                    />
                    <br />
                    <br />
                    <button>Sign Up Now</button>
                    <hr />
                </form>
            </div> 
        )
    }

}

export default GetStartedForm