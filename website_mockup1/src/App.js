import React from "react"
import NavigationBar from "./NavigationBar"
import Header from "./Header"
import Partners from "./Partners"
import HowItWorks from "./HowItWorks"
import Featured from "./Featured"
import Review from "./Review"
import GetStartedForm from "./GetStartedForm"
import Footer from "./Footer"
import {BrowserRouter} from "react-router-dom"

//Mockup application of of Upwork Website for training purposes

function App(){
    return (
        <div>
            <BrowserRouter> 
                <NavigationBar/> 
                <Header />
                <Partners />
                <Footer />
            </BrowserRouter>
       </div>
    )
}

export default App