import React from "react"

function Review(){

    const review = "This is a website Mockup !"
    const author = "Author"
    const title = "SomeTitle"
    const authorCompany = "SomeCompany"

    return (
        <div>
            <h2>{review}</h2>
            <h3>{author}</h3>
            <p>{title + ","  +  authorCompany}</p>
            <hr />
        </div>
    )
}
export default Review