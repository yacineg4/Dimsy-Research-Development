import React from "react"
import {Button,Form,Container,Row,Col} from "react-bootstrap"

class Login extends React.Component{
    render(){
        return (
            <div style={{background:"#DCDCDC"}}>
                <br />
                <Container bg="white">
                <Row>
                    <Col md={{ span: 4, offset: 4 }}>
                        <Form>
                            <Form.Group controlId="formBasicEmail">
                                <h2>Log in and get to work</h2>
                                <Form.Control type="email" placeholder="Username or Email" />
                            </Form.Group>
                            <Col md={{ span: 4, offset: 4 }}>
                                <Button variant="success" type="submit">
                                    Continue
                                </Button>
                            </Col>
                            <br />
                        </Form>
                    </Col>
                </Row>
                </Container>
            </div>
            
        )
    }
}

export default Login