import React from "react"
import {Navbar,NavItem,Button,Nav} from "react-bootstrap"
import Login from "./Login"
import SignUp from "./SignUp"
import {BrowserRouter as Router , Route , Link} from "react-router-dom"  
import "../node_modules/bootstrap/dist/css/bootstrap.min.css"

class NavigationBar extends React.Component {
    render(){
        return(
            <Router>
                <div>
                    <Navbar bg="light " expand="lg">
                        {/*Brand Should be Logo*/}
                        <Navbar.Brand style={{ color: 'green' }} href="/"><h1 style={{ color: 'green' }}>Upwork</h1></Navbar.Brand>
                        <NavItem className="navbar-nav ml-auto">
                            <Nav className="mr-auto">
                            <Nav.Link href="/Login">Log In</Nav.Link>
                            <Button  href="/SignUp" variant= "success">Get Started</Button>
                            </Nav>
                        </NavItem>
                    </Navbar>  
                </div>

                <Route path="/Login" exact component={Login} />
                <Route path="/SignUp" component={SignUp} /> 
            </Router>
    
        )
    }
}

export default NavigationBar