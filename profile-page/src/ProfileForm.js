import React from "react"

class ProfileForm extends React.Component{

    render(){

        const{
            companyname,
            services,
            email,
            phone,
            address,
            website,
            handleChange,
            handleSubmit
        } = this.props

        return(
                <form onSubmit={handleSubmit}>
                <br />
                <div className="formcontainer">
                    <input
                        className="form-control"
                        name="companyname"
                        value={companyname}
                        onChange={handleChange}
                        placeHolder="Insert your Company name ..."
                    ></input>
<br />
                    <input
                        className="form-control"
                        name="services"
                        value={services}
                        onChange={handleChange}
                        placeHolder="Insert a service ..."
                    ></input>
<br />
                    <input
                        className="form-control"
                        name="email"
                        value={email}
                        onChange={handleChange}
                        placeHolder="Insert your e-mail..."
                    ></input>
<br />
                    <input
                        className="form-control"
                        name="phone"
                        value={phone}
                        onChange={handleChange}
                        placeHolder="Insert your Company phone ..."
                    ></input>
<br />
                    <input
                        className="form-control"
                        name="address"
                        value={address}
                        onChange={handleChange}
                        placeHolder="Insert your Company address ..."
                    ></input>
<br />
                    <input
                        className="form-control"
                        name="website"
                        value={website}
                        onChange={handleChange}
                        placeHolder="Insert your Company website ..."
                    ></input>
                     <br />
                    <button className="btn btn-primary">Submit</button>
                    </div>
                    <br />
                 </form>
        )
    }
}

export default ProfileForm