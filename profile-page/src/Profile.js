import React from "react"

class Profile extends React.Component{
    render(){
        const{
            companyname,
            services,

        } = this.props

        return(
            <div className="card">
                <div className="container">
                    <h1>{companyname}</h1>
                    <p>
                        <b>Products and Services</b>
                        <br/>{services}
                    </p>
                    <h2>Siemens AG is a German multinational conglomerate 
                        company headquartered in Berlin and Munich and the largest industrial 
                        manufacturing company in Europe with branch offices abroad.
                    </h2>
                </div>
            </div>
        )
    }
}




export default Profile