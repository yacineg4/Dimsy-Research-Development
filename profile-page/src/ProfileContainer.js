import React from "react"
import Profile from "./Profile"
import ProfileForm from "./ProfileForm"
import Contact from "./Contact"

class ProfileContainer extends React.Component{
    constructor(props){
        super(props)

        this.state = {
            companyname:"",
            services:"",
            email:"",
            phone:"",
            address:"",
            website:"",
        }

    }

    handleChange = e => {
        const { name, value } = e.target
        this.setState({ [name]: value })
    }
    
    handleSubmit = e => {
        fetch("https://herokuappdjango.herokuapp.com/compagnie/",
            {
                method:'POST',
                headers: {
                    'Content-Type': 'application/json'
                  },
                body:JSON.stringify({
                    "companyname":this.companyname,
                    "services":this.services,
                    "email":this.email,
                    "phone":this.phone,
                    "address":this.address,
                    "website":this.website,
                
            }),
            }
            .then(res => res.json())
        )
    }

    render(){
        
        return (
            <div className="rowC">
                <ProfileForm 
                handleSubmit={this.handleSubmit}
                handleChange={this.handleChange}
                {...this.state}    
                />
                <br />
              <div className="rowC">
                 <Contact {...this.state}/>
                 <Profile {...this.state}/>
             </div>
            
            </div>
        )
    }
      
    
}

export default ProfileContainer