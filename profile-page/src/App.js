import React from "react"
import ProfileContainer from "./ProfileContainer"

import 'bootstrap/dist/css/bootstrap.css';
import "./styles.css"

class App extends React.Component{
    render(){
        return(
            <div>
                <ProfileContainer/>
            </div>
        )
    }
}

export default App