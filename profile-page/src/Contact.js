import React from "react"

class Contact extends React.Component{

    render(){
        const{
            email,
            phone,
            address,
            website,
        } = this.props

        return(
            <div className='card2'>
                <img className="img" src="https://pbs.twimg.com/profile_images/993408249046106112/fq_-uT1b_400x400.jpg" alt=""/>
                <div>
                    <p>{email}</p>
                    <p>{phone}</p>
                    <p>{address}</p>
                    <p>{website}</p>
                </div>
            </div>
        )
    }
}

export default Contact