import React from "react"
import Header from "./Header"
import MemeGenerator from "./MemeGenerator"

import styles from "./styles.scss"

function App(){
    return (
        <div>
            <Header />
            <MemeGenerator />
        </div>
    )
}

export default App