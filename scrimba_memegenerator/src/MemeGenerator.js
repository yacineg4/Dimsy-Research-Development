import React from "react"

class MemeGenerator extends React.Component{
    constructor(){
        super()
        this.state = {
            topText:"",
            bottomText:"",
            randomImg: "http://i.imgflip.com/1bij.jpg",
            allMemeImgs: []
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleGenClick = this.handleGenClick.bind(this)
    }

    componentDidMount(){
        fetch("https://api.imgflip.com/get_memes")
        .then(response => response.json())
        .then(response => {
                            const {memes}=response.data 
                            this.setState({allMemeImgs:memes})
                          }
        )
    }

    handleChange(event){
        const {name ,value} = event.target
        this.setState({[name]:value})

    }
     handleGenClick(event){
        
            event.preventDefault()
            const rand = this.state.allMemeImgs[Math.floor(Math.random() * this.state.allMemeImgs.length)].url
            this.setState({ randomImg: rand })
        
     }

    render(){
        return(
            <div>
                <form className = "meme-form" onSubmit={this.handleGenClick}>
                    <input 
                        name="topText" 
                        value={this.state.topText}
                        placeholder="Top text"
                        onChange={this.handleChange}
                    />
                    <input 
                        name="bottomText" 
                        value={this.state.bottomText}
                        placeholder="Bottom text"
                        onChange={this.handleChange}
                    />
                    <br />
                    <button>Generate</button>
                </form> 
                <div className = "meme"> 
                            <img src={this.state.randomImg} alt=""/>
                            <h2 className="top">{this.state.topText}</h2>
                            <h2 className="bottom">{this.state.bottomText}</h2>
                </div>
            </div>       
        )
    }
}

export default MemeGenerator