import React from "react";
import User from "utils/User"

class App extends React.Component {
  render() {
    return <User name="Jessica Doe" avatar="..." email="hello@jessica.com" />;
  }
}

export default App;