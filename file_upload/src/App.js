import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <form>
        <input
          type='file'
          placeholder='Choose a image file to upload'
          name='imagefile'
        />
      </form>
    </div>
  );
}

export default App;
